# Pure Data patches


## Audio Setup
- start jack  
The following setup will allow firefox out and pd out together:
![](./docs/jack_setup.png)
- OS audio settings:  
![](./docs/os_audio_setup.png)



## Pd settings

### Paths
`/mnt/43177221-eb12-4297-ba68-897d7eaa1293/MEGA_DATA/MUSIC_COMPOSING/Pd/externals`
`/mnt/43177221-eb12-4297-ba68-897d7eaa1293/MEGA_DATA/MUSIC_COMPOSING/Pd/externals/my`
`/usr/lib/pd/extra/Gem`

#### [Declare] (optional)
https://pd.iem.sh/objects/declare/  
http://msp.ucsd.edu/Pd_documentation/x4.htm  

### Startup
`/usr/lib/pd/extra/Gem`

### Audio
![](./docs/pd_audio_setup.png)

### Help -> Find Externals (Deken preferences)
![](docs/deken_setup.png)



## Gem
list cameras:  
`VBoxManage list webcams`
camera resolutions:  
`v4l2-ctl -d /dev/video0 --list-formats-ext`


### Camera patch
examples:
- https://orlakelleher.wordpress.com/category/pdgem/
- gem_101 to gem_106


### Gem to OBS

1. add v4l2 device
`sudo modprobe v4l2loopback devices=1 video_nr=10 card_label="OBS Cam"`

2. list devices:
`v4l2-ctl --list-devices`

3. Add remove devices
Use the v4l2loopback-ctl utility to add and remove devices while the module 
is installed using the command `v4l2loopback-ctl add /dev/video2` and
`v4l2loopback-ctl delete /dev/video2`.



## ComfyUI
GPU utilization: `nvtop`

Install custom nodes:  
- goto ComfyUI/custom_nodes dir in terminal(cmd)
- `git clone <repo-url>`


### Custom nodes
- https://github.com/ltdrdata/ComfyUI-Manager


#### AnimateDiff
https://github.com/ArtVentureX/comfyui-animatediff

Download one or more motion models from Original Models | Finetuned Models. See README for additional model links and usage. Put the model weights under ComfyUI/custom_nodes/ComfyUI-AnimateDiff-Evolved/models. You are free to rename the models, but keeping original names will ease use when sharing your workflow.
https://huggingface.co/guoyww/animatediff/tree/main  
https://huggingface.co/manshoety/AD_Stabilized_Motion/tree/main  






## General Notes

Pure Data

- PureData Tutorials - Rich Synthesis  
Really Useful Plugins  
https://www.youtube.com/playlist?list=PLqJgTfn3kSMW3AAAl2liJRKd-7DhZwLlq  


- Let's Learn Pure Data!  
Sound Simulator  
https://www.youtube.com/playlist?list=PLyFkFo29zHvD4eRftIAjcLqIXCtSo7w8g


- Pure data objects  
https://pd.iem.sh/  
https://pd.iem.sh/objects/  



Cam  
software: Linux Guvcview  

YT Gem video-tutorial  
https://www.youtube.com/watch?v=Vyp6_SJlWAs  
https://www.youtube.com/watch?v=y1rBa_STq64&list=PLyFkFo29zHvD4eRftIAjcLqIXCtSo7w8g&index=18  

https://orlakelleher.wordpress.com/category/pdgem/#jp-carousel-257  



